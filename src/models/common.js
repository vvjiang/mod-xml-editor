export default {
  namespace: 'common',
  state: {
    tagsList: [], // 约束规则列表
    fileTree: [], // 左侧文件树结构
    currentTags: {}, // 当前约束规则对象
    currentTagsName: '',
    currentText: '', // 当前编辑的文本
    haveSyncText: true,// 当前编辑的文本是否已保存到本地
    haveSelectXml:false,// 是否选中了xml文件
    currentNode: {},
    isShowEditTagsModal: false // 调整约束规则的弹窗
  },
  reducers: {
    setField(state, { payload }) {
      return { ...state, ...payload };
    }
  }
}