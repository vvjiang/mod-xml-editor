import React, { useState } from 'react'
import { Button, message, Select, Tabs } from 'antd'
import connect from '@/utils/easyDva'
import Store from '@/utils/store'
import EditableAttrs from '@/component/editableAttrs'
import EditableKeyValue from './editableKeyValue'

import styles from './index.less'

const { TabPane } = Tabs
const { Option } = Select

/**
 * 调整当前约束规则面板
 */
function EditRuleModal(props) {
	const { currentTags={}, currentTagsName, setField, onReLint, tagsList } = props

	// 全局忽略的属性
	const [attrs, setAttrs] = useState(currentTags['!ignoreAttrs'] || [])
	// 指定标签忽略的属性
	const [tagAttrs, setTagAttrs] = useState(currentTags['!ignoreTagAttrs'] || [])
	// 选定同步的目标约束规则
	const [copyRule, setCopyRule] = useState(undefined)

	// 保存当前规则，并重新校验
	const saveCurrentTags = (newCurrentTags) => {
		setField({
			currentTags: {
				...newCurrentTags
			}
		})
		Store.saveTags(currentTagsName, newCurrentTags)
		message.success('同步成功')
		onReLint()
	}

	// 将全局忽略的属性同步到约束规则的tags中(不保存)
	const syncAttrsToTagsWithoutSave = (currentTags, attrs) => {
		currentTags['!ignoreAttrs'] = attrs
		// 首先获取tags中所有的标签名，!开头的不是标签名，是全局设置或者调整规则
		const tagNames = Object.keys(currentTags).filter(key => key[0] !== '!')
		// 遍历tags中的元素，将属性置为null
		tagNames.forEach(tagName => {
			const tagObj = currentTags[tagName]
			if (tagObj && tagObj.attrs) {
				attrs.forEach(ignoreAttrName => {
					if (tagObj.attrs[ignoreAttrName]) {
						tagObj.attrs[ignoreAttrName] = null
					}
				})
			}
		})
		return currentTags
	}

	// 将全局忽略的属性同步到约束规则的tags中
	const syncAttrsToTags = () => {
		const newCurrentTags = syncAttrsToTagsWithoutSave(currentTags, attrs)
		saveCurrentTags(newCurrentTags)
	}

	// 将指定标签忽略的属性同步到约束规则的tags中(不保存)
	const syncTagAttrsToTagsWithoutSave = (currentTags, tagAttrs) => {
		currentTags['!ignoreTagAttrs'] = tagAttrs

		tagAttrs.forEach(item => {
			const tagObj = currentTags[item.tagName]

			if (tagObj && tagObj.attrs && tagObj.attrs[item.attrName]) {
				tagObj.attrs[item.attrName] = null
			}
		})
		return currentTags
	}

	// 将指定标签忽略的属性同步到约束规则的tags中
	const syncTagAttrsToTags = () => {
		const newCurrentTags = syncTagAttrsToTagsWithoutSave(currentTags, tagAttrs)
		saveCurrentTags(newCurrentTags)
	}

	// 同步其他约束规则的手动调整规则，到当前约束规则中
	const syncOtherRule = () => {
		if (!copyRule) {
			message.error('请选择其他约束规则')
			return
		}
		const copyRuleTags = tagsList.find(l => l.name === copyRule).tags
		let newCurrentTags = currentTags
		// 处理全局忽略属性
		const ignoreAttrs = copyRuleTags['!ignoreAttrs']
		if (ignoreAttrs) {
			// 合并规则
			const newAttrs = Array.from(new Set([...ignoreAttrs, ...attrs]))
			setAttrs(newAttrs)
			newCurrentTags = syncAttrsToTagsWithoutSave(newCurrentTags, newAttrs)
		}
		// 处理指定标签忽略属性
		const ignoreTagAttrs = copyRuleTags['!ignoreTagAttrs']
		if (ignoreTagAttrs) {
			// 合并
			const newTagAttrs = [...ignoreTagAttrs, ...tagAttrs]
			// 去重
			const result = []
			const tempObj = {}
			newTagAttrs.forEach(item => {
				const key = `${item.tagName}_dist_${item.attrName}`
				if (!tempObj[key]) {
					tempObj[key] = 1
					result.push(item)
				}
			})
			setTagAttrs(result)
			newCurrentTags = syncTagAttrsToTagsWithoutSave(newCurrentTags, result)
		}

		saveCurrentTags(newCurrentTags)
	}

	return (
		<div className={styles.container}>
			<Tabs defaultActiveKey="1" tabBarExtraContent={`当前正在调整的约束规则：${currentTagsName}`}>
				<TabPane tab="指定标签忽略检测" key="1">
					<div className={styles.tabHeader}>
						添加到下列表格中的指定标签下的属性，将不再检测它的值是否符合约束，点击 <Button type="primary" size="small" onClick={syncTagAttrsToTags}>同步到约束规则</Button> 后生效
					</div>
					<EditableKeyValue value={tagAttrs} onChange={setTagAttrs} />
				</TabPane>
				<TabPane tab="全局忽略检测" key="2">
					<div className={styles.tabHeader}>
						点击 <Button type="primary" size="small" onClick={syncAttrsToTags}>同步到约束规则</Button> 后，以下属性将不再检测它的值是否符合约束
					</div>
					<EditableAttrs value={attrs} onChange={setAttrs} />
				</TabPane>
				<TabPane tab="同步调整" key="3">
					<div className={styles.tabHeader}>
						点击 <Button type="primary" size="small" onClick={syncOtherRule}>同步调整规则</Button> 后，将选择的约束规则的手动调整规则，同步到当前约束规则中
					</div>
					<Select placeholder="请选择约束规则" size="small" value={copyRule} style={{ width: 200 }} onChange={setCopyRule}>
						{
							tagsList.filter(l => l.name !== currentTagsName).map((tagsItem) => {
								return <Option key={tagsItem.name} >{tagsItem.name}</Option>
							})
						}
					</Select>
				</TabPane>
			</Tabs>
		</div>
	)
}

export default connect(EditRuleModal)