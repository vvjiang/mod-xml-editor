import React from 'react'
import { Button, Divider } from 'antd'
import FileInput from '@/component/fileInput'
import styles from './index.less'

export default function (props) {
  const { onNext, onSelectDir, dirHandleList } = props

  const fileChange = (dirHandle) => {
    onSelectDir([...dirHandleList, dirHandle])
  }

  return <div>
    <div>
      <FileInput onChange={fileChange}>
        <Button type="primary">选择文件夹</Button>
      </FileInput>
    </div>
    {dirHandleList.length > 0 && <div className={styles['select-dir-total']}>已选择{dirHandleList.map(l=>l.name).join('，')}等<i>{dirHandleList.length}</i>个文件夹</div>}
    {
      dirHandleList.length > 0 && <div className={styles['modal-footer']}>
        <Button type="primary" onClick={onNext}>下一步</Button>
      </div>
    }
    {
      dirHandleList.length === 0 && <>
        <Divider plain>提示</Divider>
        <ol className={styles.tips}>
          <li>请选择xml文件所在的文件夹</li>
          <li>选择该文件夹之后，会读取文件夹下所有的xml文件，并进行学习</li>
          <li>可通过多次选取，选择多个文件夹的文件</li>
        </ol>
      </>
    }
  </div >
}
