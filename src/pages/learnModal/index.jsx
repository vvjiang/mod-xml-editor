import React, { useState } from 'react'
import { Modal, Steps, message } from 'antd'
import Store from '@/utils/store/index'
import connect from '@/utils/easyDva'
import StepSelectDir from './stepSelectDir'
import StepLearnXml from './stepLearnXml'
import StepSave from './stepSave'
import styles from './index.less'

const { Step } = Steps;
/**
 * 学习XML生成约束规则的弹框
 */
function LearnModal(props) {
  const { onClose, setField } = props
  const [dirHandleList, setDirHandleList] = useState([])// 选择的文件夹列表
  const [step, setStep] = useState(0)// 阶段
  const [tags, setTags] = useState([])

  const handleSave = (name) => {
    Store.saveTags(name, tags).then(() => {
      message.success('保存成功')
      Store.getTagsList().then((tagsList) => {
        setField({
          tagsList
        })
        setStep(0)
        onClose()
      })
    })
  }

  return (
    <Modal
      title="新增约束规则"
      visible={true}
      onCancel={onClose}
      width={1000}
      maskClosable={false}
      zIndex={10}
      footer={null}
    >
      <div className={styles['modal-container']}>
        <Steps current={step}>
          <Step key='1' title="选择文件夹" />
          <Step key='2' title="学习文件" />
          <Step key='3' title="保存" />
        </Steps>
        <div className={styles['modal-content']}>
          {step === 0 && <StepSelectDir dirHandleList={dirHandleList} onSelectDir={setDirHandleList} onNext={() => { setStep(1) }} />}
          {step === 1 && <StepLearnXml dirHandleList={dirHandleList} onNext={(tags) => {
            setTags(tags)
            setStep(2)
          }} />}
          {step === 2 && <StepSave onSave={handleSave} />}
        </div>
      </div>
    </Modal>
  );
}

export default connect(LearnModal)