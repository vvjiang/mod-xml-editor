import React from 'react';
import { Button, Form, Input, Divider } from 'antd';
import styles from './index.less';

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 8 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 8 },
};

export default function (props) {
  const { onSave } = props
  const onFinish = (values) => {
    onSave(values.tagsName)
  }

  return <div>
    <Form
      {...layout}
      name="basic"
      onFinish={onFinish}
    >
      <Form.Item
        label="约束规则名称"
        name="tagsName"
        rules={[{ required: true, message: '请输入约束规则名称!' }]}
      >
        <Input placeholder="请输入约束规则名称" />
      </Form.Item>

      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">
          保存
        </Button>
      </Form.Item>
    </Form>
    <Divider plain>注意</Divider>
    <ol className={styles.tips}>
      <li>点击保存后，将新增一份约束规则，可在主页进行选择</li>
      <li>约束规则的数据将存储于本地浏览器中，不会上传至服务器</li>
      <li>如果遇到非常特殊的情况（即约束规则过大，过多），导致浏览器本身存储空间（5M）不够用，请手动清理不常用数据</li>
    </ol>
  </div>
}
