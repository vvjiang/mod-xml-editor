import { lintFileTree } from '@/utils/files'

onmessage = ({ data }) => {
  lintFileTree(data.fileTree, data.currentTags).then(content => {
    postMessage(content)
  })
}

