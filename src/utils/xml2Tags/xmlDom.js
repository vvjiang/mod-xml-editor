import { DOMParser } from 'xmldom'
import XMLNodeTypes from './xmlNodeTypes'
// import XsdAttributes from './xsdAttributes'

/**
 * 根据xml文本创建xml的DOM结构
 */
const createXmlDom = (xml) => {
    return new DOMParser().parseFromString(xml, 'text/xml');
}

/**
 * 获取Xml中元素节点上的属性数组
 */
const getElementAttributes = (ele) => {
    if (ele.attributes.length === 0) {
        return []
    }
    return Array.from(ele.attributes).map(item => {
        return {
            name: item.name,
            value: item.value
        }
    })
}

/**
 * 获取Xml中元素节点下的元素节点名称数组 
 */
const getElementChildren = (ele) => {
    const childNodes = ele.childNodes
    if (!childNodes || childNodes.length === 0) {
        return []
    }
    const childElements=Array.from(childNodes).filter(l=>l.nodeType===XMLNodeTypes.ELEMENT_NODE)
    return childElements
}

export default {
    createXmlDom,
    getElementAttributes,
    getElementChildren
}