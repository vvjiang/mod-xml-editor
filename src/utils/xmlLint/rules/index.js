// 属性值一定不可重复
export { default as attrNoDuplication } from './attr-no-duplication'
// 属性值必须放在双引号中
export { default as attrValueDoubleQuotes } from './attr-value-double-quotes'
// 属性值必须放在单引号中
export { default as attrValueSingleQuotes } from './attr-value-single-quotes'
// 属性值一定不可为空
export { default as attrValueNotEmpty } from './attr-value-not-empty'
// 空格和制表符一定不可混合在行前
export { default as spaceTabMixedDisabled } from './space-tab-mixed-disabled'
// 特殊字符必须转义
export { default as specCharEscape } from './spec-char-escape'
// 标签必须成对
export { default as tagPair } from './tag-pair'
// 所有属性只能用一个空格隔开，并且不能有前导/尾随空格。
export { default as attrWhitespace } from './attr-whitespace'
// 属性名称和值之间没有空格
export { default as attrNoUnnecessaryWhitespace } from './attr-no-unnecessary-whitespace'

export { default as tagsCheck } from './tags-check'

