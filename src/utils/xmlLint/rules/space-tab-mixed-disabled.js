

export default {
  id: 'space-tab-mixed-disabled',
  description: '缩进不能混用tab和空格',
  init(parser, reporter, options) {
    let indentMode = 'nomix'
    let spaceLengthRequire = null

    if (typeof options === 'string') {
      const match = /^([a-z]+)(\d+)?/.exec(options)
      if (match) {
        indentMode = match[1]
        spaceLengthRequire = match[2] && parseInt(match[2], 10)
      }
    }

    parser.addListener('text', (event) => {
      const raw = event.raw
      const reMixed = /(^|\r?\n)([ \t]+)/g
      let match

      while ((match = reMixed.exec(raw))) {
        const fixedPos = parser.fixPos(event, match.index + match[1].length)
        if (fixedPos.col !== 1) {
          continue
        }

        const whiteSpace = match[2]
        if (indentMode === 'space') {
          if (spaceLengthRequire) {
            if (
              /^ +$/.test(whiteSpace) === false ||
              whiteSpace.length % spaceLengthRequire !== 0
            ) {
              reporter.warn(
                `请使用空格缩进，并保持空格长度为${spaceLengthRequire}`,
                fixedPos.line,
                1,
                this,
                event.raw
              )
            }
          } else {
            if (/^ +$/.test(whiteSpace) === false) {
              reporter.warn(
                '请使用空格缩进',
                fixedPos.line,
                1,
                this,
                event.raw
              )
            }
          }
        } else if (indentMode === 'tab' && /^\t+$/.test(whiteSpace) === false) {
          reporter.warn(
            '请使用tab缩进',
            fixedPos.line,
            1,
            this,
            event.raw
          )
        } else if (/ +\t|\t+ /.test(whiteSpace) === true) {
          reporter.warn(
            '缩进不能混用tab和空格',
            fixedPos.line,
            1,
            this,
            event.raw
          )
        }
      }
    })
  },
}
