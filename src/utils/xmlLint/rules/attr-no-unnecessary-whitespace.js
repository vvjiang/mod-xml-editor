
export default {
  id: 'attr-no-unnecessary-whitespace',
  description: '属性名和值之间不能有空格',
  init(parser, reporter, options) {
    const exceptions = Array.isArray(options) ? options : []

    parser.addListener('tagstart', (event) => {
      const attrs = event.attrs
      const col = event.col + event.tagName.length + 1

      for (let i = 0; i < attrs.length; i++) {
        if (exceptions.indexOf(attrs[i].name) === -1) {
          const match = /(\s*)=(\s*)/.exec(attrs[i].raw.trim())
          if (match && (match[1].length !== 0 || match[2].length !== 0)) {
            reporter.error(
              `在属性'${attrs[i].name}'的属性名和值之间不能有空格`,
              event.line,
              col + attrs[i].index,
              this,
              attrs[i].raw
            )
          }
        }
      }
    })
  },
}
