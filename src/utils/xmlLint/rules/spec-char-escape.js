

export default {
  id: 'spec-char-escape',
  description: '特殊字符必须被转义',
  init(parser, reporter) {
    parser.addListener('text', (event) => {
      const raw = event.raw
      // TODO: improve use-cases for &
      const reSpecChar = /([<>])|( \& )/g
      let match

      while ((match = reSpecChar.exec(raw))) {
        const fixedPos = parser.fixPos(event, match.index)
        reporter.error(
          `特殊字符必须被转义 : [ ${match[0]} ].`,
          fixedPos.line,
          fixedPos.col,
          this,
          event.raw
        )
      }
    })
  },
}
