

export default {
  id: 'attr-value-not-empty',
  description: '所有属性必须有值',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const attrs = event.attrs
      let attr
      const col = event.col + event.tagName.length + 1

      for (let i = 0, l = attrs.length; i < l; i++) {
        attr = attrs[i]

        if (attr.quote === '' && attr.value === '') {
          reporter.warn(
            `属性[ ${attr.name} ]必须有值`,
            event.line,
            col + attr.index,
            this,
            attr.raw
          )
        }
      }
    })
  },
}
