export default {
  id: 'attr-no-duplication',
  description: '标签上不能有重复的属性',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const attrs = event.attrs
      let attr
      let attrName
      const col = event.col + event.tagName.length + 1

      const mapAttrName = {}

      for (let i = 0, l = attrs.length; i < l; i++) {
        attr = attrs[i]
        attrName = attr.name

        if (mapAttrName[attrName] === true) {
          reporter.error(
            `重复的属性名： [ ${attr.name} ]`,
            event.line,
            col + attr.index,
            this,
            attr.raw
          )
        }
        mapAttrName[attrName] = true
      }
    })
  },
}
