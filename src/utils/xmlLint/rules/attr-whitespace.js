

export default {
  id: 'attr-whitespace',
  description:
    '所有属性只能用一个空格隔开，并且不能有前导/尾随空格.',
  init(parser, reporter, options) {
    const exceptions = Array.isArray(options)
      ? options
      : []

    parser.addListener('tagstart', (event) => {
      const attrs = event.attrs
      let attr
      const col = event.col + event.tagName.length + 1

      attrs.forEach((elem) => {
        attr = elem
        const attrName = elem.name

        if (exceptions.indexOf(attrName) !== -1) {
          return
        }

        // Check first and last characters for spaces
        if (elem.value.trim() !== elem.value) {
          reporter.error(
            `属性[ ${attrName} ]不能有前导/尾随空格.`,
            event.line,
            col + attr.index,
            this,
            attr.raw
          )
        }

        if (elem.value.replace(/ +(?= )/g, '') !== elem.value) {
          reporter.error(
            `属性[ ${attrName} ]只能用一个空格隔开`,
            event.line,
            col + attr.index,
            this,
            attr.raw
          )
        }
      })
    })
  },
}
