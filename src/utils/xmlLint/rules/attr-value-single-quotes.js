

export default {
  id: 'attr-value-single-quotes',
  description: '属性值必须在单引号之内',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const attrs = event.attrs
      let attr
      const col = event.col + event.tagName.length + 1

      for (let i = 0, l = attrs.length; i < l; i++) {
        attr = attrs[i]

        if (
          (attr.value !== '' && attr.quote !== "'") ||
          (attr.value === '' && attr.quote === '"')
        ) {
          reporter.error(
            `属性[ ${attr.name} ]的值必须在单引号之内`,
            event.line,
            col + attr.index,
            this,
            attr.raw
          )
        }
      }
    })
  },
}
