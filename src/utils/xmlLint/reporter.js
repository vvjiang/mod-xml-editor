const ReportType = {
  error: 'error',
  warning: 'warning',
  info: 'info'
}

export default class Reporter {
  html
  lines
  brLen
  ruleset
  messages

  constructor(html, ruleset) {
    this.html = html
    this.lines = html.split(/\r?\n/)
    const match = /\r?\n/.exec(html)

    this.brLen = match !== null ? match[0].length : 0
    this.ruleset = ruleset
    this.messages = []
  }

  info(
    message,
    line,
    col,
    rule,
    raw
  ) {
    this.report(ReportType.info, message, line, col, rule, raw)
  }

  warn(
    message,
    line,
    col,
    rule,
    raw
  ) {
    this.report(ReportType.warning, message, line, col, rule, raw)
  }

  error(
    message,
    line,
    col,
    rule,
    raw
  ) {
    this.report(ReportType.error, message, line, col, rule, raw)
  }

  report(
    type,
    message,
    line,
    col,
    rule,
    raw
  ) {
    const lines = this.lines
    const brLen = this.brLen
    let evidence = ''
    let evidenceLen = 0

    for (let i = line - 1, lineCount = lines.length; i < lineCount; i++) {
      evidence = lines[i]
      evidenceLen = evidence.length
      if (col > evidenceLen && line < lineCount) {
        line++
        col -= evidenceLen
        if (col !== 1) {
          col -= brLen
        }
      } else {
        break
      }
    }

    this.messages.push({
      type: type,
      message: message,
      raw: raw,
      evidence: evidence,
      line: line,
      col: col,
      rule: {
        id: rule.id,
        description: rule.description,
        link: `https://github.com/thedaviddias/HTMLHint/wiki/${rule.id}`,
      }
    })
  }
}