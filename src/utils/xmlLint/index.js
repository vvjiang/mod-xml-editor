import XMLParser from './xmlparser'
import Reporter from './reporter'
import * as XMLRules from './rules'

class XMLLintCore {
  rules = {}
  defaultRuleset = {
    'attr-no-duplication': true,
    'attr-no-unnecessary-whitespace': true,
    // 'attr-value-double-quotes': true,
    'attr-value-not-empty': true,
    // 'attr-value-single-quotes': false,
    'attr-whitespace': true,
    'space-tab-mixed-disabled': true,
    'spec-char-escape': true,
    'tag-pair': true,
    'tags-check': true,
  }

  addRule(rule) {
    this.rules[rule.id] = rule
  }

  verify(xml, ruleset = this.defaultRuleset, tags) {
    if (Object.keys(ruleset).length === 0) {
      ruleset = this.defaultRuleset
    }

    const parser = new XMLParser()
    const reporter = new Reporter(xml, ruleset)

    const rules = this.rules
    let rule

    for (const id in ruleset) {
      rule = rules[id]
      if (rule !== undefined && ruleset[id] !== false) {
        rule.init(parser, reporter, ruleset[id], tags)
      }
    }

    parser.parse(xml)

    return reporter.messages
  }

  format(arrMessages, options) {
    const arrLogs = []
    const colors = {
      white: '',
      grey: '',
      red: '',
      reset: '',
    }

    if (options.colors) {
      colors.white = '\x1b[37m'
      colors.grey = '\x1b[90m'
      colors.red = '\x1b[31m'
      colors.reset = '\x1b[39m'
    }

    const indent = options.indent || 0

    arrMessages.forEach((hint) => {
      const leftWindow = 40
      const rightWindow = leftWindow + 20
      let evidence = hint.evidence
      const line = hint.line
      const col = hint.col
      const evidenceCount = evidence.length
      let leftCol = col > leftWindow + 1 ? col - leftWindow : 1
      let rightCol =
        evidence.length > col + rightWindow ? col + rightWindow : evidenceCount

      if (col < leftWindow + 1) {
        rightCol += leftWindow - col + 1
      }

      evidence = evidence.replace(/\t/g, ' ').substring(leftCol - 1, rightCol)

      // add ...
      if (leftCol > 1) {
        evidence = `...${evidence}`
        leftCol -= 3
      }
      if (rightCol < evidenceCount) {
        evidence += '...'
      }

      // show evidence
      arrLogs.push(
        `${colors.white + repeatStr(indent)}L${line} |${
        colors.grey
        }${evidence}${colors.reset}`
      )

      // show pointer & message
      let pointCol = col - leftCol
      // add double byte character
      // eslint-disable-next-line no-control-regex
      const match = evidence.substring(0, pointCol).match(/[^\u0000-\u00ff]/g)
      if (match !== null) {
        pointCol += match.length
      }

      arrLogs.push(
        `${
        colors.white +
        repeatStr(indent) +
        repeatStr(String(line).length + 3 + pointCol)
        }^ ${colors.red}${hint.message} (${hint.rule.id})${colors.reset}`
      )
    })

    return arrLogs
  }
}

// repeat string
function repeatStr(n, str) {
  return new Array(n + 1).join(str || ' ')
}

export const XMLLint = new XMLLintCore()

Object.keys(XMLRules).forEach((key) => {
  XMLLint.addRule(XMLRules[key])
})

export { XMLRules, Reporter, XMLParser }