
import { connect } from 'dva'

const mapDispatchToProps = (dispatch) => {
  return {
    setField: (payload) => {
      dispatch({
        type: 'common/setField',
        payload: payload
      });
    }
  }
}
const mapStateToProps = ({ common }) => {
  return {
    ...common
  }
}
// 简化dva的数据流
const newConnect = (component) => {
  return connect(mapStateToProps, mapDispatchToProps)(component)
}

export default newConnect