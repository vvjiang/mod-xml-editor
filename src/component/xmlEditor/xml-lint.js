
import { XMLLint } from '@/utils/xmlLint'

// 向CodeMirror注册XML代码校验功能
const CodeMirrorRegisterXmlLint = (CodeMirror, tags, onError) => {
  CodeMirror.registerHelper("lint", "xml", function (text, rules) {
    var found = [];
    var messages = XMLLint.verify(text, rules, tags)
    for (var i = 0; i < messages.length; i++) {
      var message = messages[i];
      var startLine = message.line - 1, endLine = message.line - 1, startCol = message.col - 1, endCol = message.col;
      found.push({
        from: CodeMirror.Pos(startLine, startCol),
        to: CodeMirror.Pos(endLine, endCol),
        message: message.message,
        severity: message.type
      });
    }
    if (onError) {
      onError(found)
    }
    return found;
  });
}

export default CodeMirrorRegisterXmlLint