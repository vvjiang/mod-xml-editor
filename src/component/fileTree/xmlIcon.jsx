import Icon from '@ant-design/icons'

const HeartSvg = () => (
  <svg width="1em" height="1em" viewBox="0 0 1024 1024" fill="currentColor">
    <path d="M549.973333 128l83.626667 17.066667L474.026667 896l-83.626667-17.066667L549.973333 128m285.866667 384L682.666667 358.826667V238.08L956.586667 512 682.666667 785.493333v-120.746666L835.84 512M67.413333 512L341.333333 238.08v120.746667L188.16 512 341.333333 664.746667v120.746666L67.413333 512z" ></path>
  </svg>
);

const XmlIcon = (props) => {
  return <Icon component={HeartSvg} {...props} />
}

export default XmlIcon
