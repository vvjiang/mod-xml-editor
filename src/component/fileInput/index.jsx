import React from 'react'

// 自定义的文件夹树形组件
const FileInput = (props) => {
  const { children, onChange } = props
  const handleClick = async () => {
    const dirHandle = await window.showDirectoryPicker()
    dirHandle.requestPermission({ mode : "readwrite" })
    onChange(dirHandle)
  }
  return <span onClick={handleClick}>
    {children}
  </span>
}

export default FileInput
