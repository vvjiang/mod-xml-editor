# ModXmlEditor

## 介绍

一个编辑MOD中XML文件的编辑器

体验网站：[https://vvjiang.gitee.io/mod-xml-editor/](https://vvjiang.gitee.io/mod-xml-editor/)

## 功能

* 学习XML生成约束规则，支持代码提示和代码校验
* 格式化，注释，回退，选中文本按Tab，寻常编辑器的操作应有尽有
* 对于只想使用的朋友，dist目录就是网站成品，部署一下本地网站
* 本项目未考虑兼容性，推荐Edge和Chrome浏览器，用IE顶不住
* 本项目纯前端，数据存储于浏览器本地，无需联网
* 支持新建文件夹和文件（需要Chrome 86+）
* 支持修改和保存文件（需要Chrome 86+）

## 使用说明：

  * 新增约束规则
    * 选择指定的包含xml文件的文件夹。
    * 开始自动学习xml文件
    * 输入约束规则名称，并保存在浏览器本地存储中
  * 选择约束规则
  * 选择约束规则后，可以手动调整约束规则
  * 打开指定的想要编辑的文件夹即可
  * 请使用https的前缀打开网站

完成以上操作后，便可以正常使用xml编辑器了

## 安装【以下内容仅限于想了解代码结构的，其余退散】

    npm i

## 运行框架

    npm start






