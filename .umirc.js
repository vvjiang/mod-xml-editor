// ref: https://umijs.org/config/
const workerLoader = require('worker-loader');

export default {
  base: '/mod-xml-editor/',
  publicPath: './',
  treeShaking: true,
  routes: [
    {
      path: '/',
      component: '../layouts/index',
      routes: [{ path: '/', component: '../pages/index' }],
    },
  ],
  chainWebpack(memo) {
    memo.module
      .rule('worker-loader')
      .test(/\.webworker\.js$/)
      .use(require.resolve('worker-loader'))
      .loader(require.resolve('worker-loader'))
      .options({
        inline: 'fallback',
      });
  },
  plugins: [
    // ref: https://umijs.org/plugin/umi-plugin-react.html
    [
      'umi-plugin-react',
      {
        antd: true,
        dva: true,
        dynamicImport: { webpackChunkName: true },
        title: 'xmlEditor',
        dll: true,
        locale: {
          enable: true,
          default: 'en-US',
        },
        routes: {
          exclude: [
            /models\//,
            /services\//,
            /model\.(t|j)sx?$/,
            /service\.(t|j)sx?$/,
            /components\//,
          ],
        },
      },
    ],
  ],
  theme: {
    'primary-color': '#0e639c',
  },
};
